

#ifndef __HardCodedData_H__
#define __HardCodedData_H__



//// DEFINE///
#define TIMEOUT_IN_MILLISECONDS 5000
#define BRUTAL_TERMINATION_CODE 0x55

#define MESSGAE_BUFFER_LEN		10
#define BUFFER_ALLOCTION_STR	1
#define OFFSET_NEIGHBOUR		1
#define MIN_TREE_NUM			2

#define TREE	'T'
#define GROUND  'G'
#define FIRE	'F'

#endif // __HardCodedData_H__