#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include "task.h"

int main(int argc, char *argv[])
{
	
	struct forest env = { 0 };
	init_data(&env, argc, argv);
	gen_forest(&env);
	freeall(&env);

	return 0;
}