

#include "task.h"

void freeall(struct forest *env) {
	fclose(env->input);
	fclose(env->output);
	free(env->forest_gen);
	free(env->forest);
}



void init_data(struct forest *env, int argc, char **argv) {
	char str[MESSGAE_BUFFER_LEN];
	char *forest_gen;
	
	env->input = fopen(argv[1], "r");
	if ((env->input == NULL)) {
		printf("Error opening input file");
		exit(-1);
	}

	env->output = fopen(argv[2], "w");
	if ((env->output == NULL)) {
		printf("Error opening output file");
		fclose(env->input);
		exit(-1);
	}
	fgets(str, MESSGAE_BUFFER_LEN, env->input);
	env->demention = strtol(str, NULL, MESSGAE_BUFFER_LEN);

	fgets(str, MESSGAE_BUFFER_LEN, env->input);
	env->roots = strtol(str, NULL, MESSGAE_BUFFER_LEN);
	
	forest_gen = (char*)malloc(((size_t)(env->demention) * (size_t)(env->demention) + BUFFER_ALLOCTION_STR) * sizeof(char));
	if (forest_gen == NULL) {
		printf("Error Allocating Memory");
		fclose(env->input);
		fclose(env->output);
		exit(-1);
	}


	env->forest_gen = forest_gen;

	env->forest=file2data(env);


}



void printstring(struct forest *env,int count) {
	fprintf(env->output,"%s - %d\n", env->forest ,count);
}

void gen_ground(struct forest *env, int place) {
	
	int up = place - (env->demention);
	int down = place + (env->demention);
	int left = place - OFFSET_NEIGHBOUR; //define offset
	int right = place + OFFSET_NEIGHBOUR;
	int up_right = up + OFFSET_NEIGHBOUR;
	int up_left = up - OFFSET_NEIGHBOUR;
	int down_left = down - OFFSET_NEIGHBOUR;
	int down_right = down + OFFSET_NEIGHBOUR;
	int col = place % env->demention;
	int row = place / env->demention;
	int count = 0;

	//write a function for secand if much easy
	if (row- OFFSET_NEIGHBOUR >= 0) {
		if (env->forest[up] == TREE) {
			count++;
		}
		if (col < env->demention - OFFSET_NEIGHBOUR) { // if it has place to the up right
			if (env->forest[up_right] == TREE) {
				count++;
			}
		}
		if (col > 0) { // if it has place to the up left
			if (env->forest[up_left] == TREE) {
				count++;
			}
		}
	}
	if (row+ OFFSET_NEIGHBOUR < env->demention) {
		if (env->forest[down] == TREE) {
			count++;
		}
		if (col < env->demention - OFFSET_NEIGHBOUR) { // if it has place to the bottom right
			if (env->forest[down_right] == TREE) {
				count++;
			}
		}
		if (col > 0) { // if it has place to the botoom left
			if (env->forest[down_left] == TREE) {
				count++;
			}
		}
	}

	if (col < env->demention - OFFSET_NEIGHBOUR) { // if it has place to the right
		if (env->forest[right] == TREE) {
			count++;
		}
	}

	if (col > 0) { // if it has place to the left
		if (env->forest[left] == TREE) {
			count++;
		}
	}
	if (count >= MIN_TREE_NUM) {
		env->forest_gen[place] = TREE;
		return;
	}

	env->forest_gen[place] = GROUND;

}

void gen_fire(struct forest *env, int place) {
	env->forest_gen[place] = GROUND;
}

void gen_tree(struct forest *env, int place) {

	int up = place - (env->demention);
	int down = place + (env->demention);
	int left = place - OFFSET_NEIGHBOUR; //define offset
	int right = place + OFFSET_NEIGHBOUR;
	int col = place % env->demention;
	int row = place / env->demention;
	
	//write a function for secand if much easy
	if (row- OFFSET_NEIGHBOUR >= 0) {
		if (env->forest[up] == FIRE) {
			env->forest_gen[place] = FIRE;
			return;
		}
	}

	if (col<env->demention- OFFSET_NEIGHBOUR) {
		if (env->forest[right] == FIRE) {
			env->forest_gen[place] = FIRE;
			return;
		}
	}

	if (row+ OFFSET_NEIGHBOUR < env->demention) {
		if (env->forest[down] == FIRE) {
			env->forest_gen[place] = FIRE;
			return;
		}
	}

	if (col> 0) {
		if (env->forest[left] == FIRE) {
			env->forest_gen[place] = FIRE;
			return;
		}
	}

	env->forest_gen[place] = TREE;
}


void gen(struct forest *env,int place) {
	
	char letter = env->forest[place];
	switch (letter) {
	case GROUND: gen_ground(env,place); break;
	case FIRE:gen_fire(env, place ); break;
	case TREE: gen_tree(env,place); break;
	}
}



char *file2data(struct forest *env)
{
	int dem = env->demention;
	FILE* input = env->input;
	char *forest = "NULL";
	char letter ;
	int rows = 0, collums = 0, place = 0;
	enum os_err Error = No_Error;


	forest = (char*)malloc(((size_t)dem * (size_t)dem + BUFFER_ALLOCTION_STR) * sizeof(char));
	if (forest == NULL) {
		printf("Error Allocating Memory");
		fclose(env->input);
		fclose(env->output);
		free(env->forest_gen);
		exit(-1);
	}

	while(rows<dem){
		letter = fgetc(input);
	
		if (letter == EOF) {
				rows++;
				continue;
			
		}
		else if (letter == ',') {
			continue;
		}
		else if (letter == '\n') {
			if (collums != (dem)) {
				Error = Error_demention;
			}
			else {
				collums = 0;
				rows++;
				continue;
			}
		}
		else {
			if (letter != GROUND &&
				letter != TREE &&
				letter != FIRE) {
				Error = Error_letter;
			}
		}
		
		if (Error != 0) {
			printf("An error has happend");
			freeall(env);
			exit (-1);
		}
		else {
			forest[place] = letter;
			place++;
			collums++;
		}
	}
	forest[place] = '\0';
	return forest;


}

 void gen_forest(struct forest *env) {
	int place = 0;
	int gen_number = 0;
	int num_of_burning_trees = 0;
	
	while (gen_number < env->roots) {


		num_of_burning_trees = burning_trees(env);
		printstring(env,num_of_burning_trees);
		for (place = 0; place < (env->demention)*(env->demention) ; place++) {
				gen(env,place);
			
		}
		env->forest_gen[place] = '\0';
		strcpy(env->forest, env->forest_gen);
		gen_number++;
	}

}


int burning_trees(struct forest* env) {

	int count_burning_trees = 0;

	count_burning_trees = CreateProcessSimpleMain(env->forest);
	if (count_burning_trees < 0) {
		printf("Error using Son.exe");
		freeall(env);
		exit(-1);
	}
	return count_burning_trees;
}


int CreateProcessSimpleMain(char *p_str)
{
	PROCESS_INFORMATION procinfo;
	DWORD				waitcode;
	DWORD				exitcode;
	BOOL				retVal;
	TCHAR				*command;
	char				*command_chr;
	
	int flag_size_tchar_2 = 0; 										/*  Start the child process. */
	char son_path[10];
	strcpy(son_path, "son.exe \0");
	int len_p_str = (int)strlen(p_str);
	int len_tot = len_p_str + (int)strlen(son_path) +1;
	command_chr = (char*)malloc(len_tot * sizeof(char));
	if (command_chr == NULL) {
		printf("Error allocating memory");
		return -1;
	}
	strcpy(command_chr, son_path);
	strncat(command_chr, p_str, len_p_str);
	
	if (sizeof(TCHAR) == 2) {
		flag_size_tchar_2 = 1;
		command = (TCHAR*)calloc(len_tot, sizeof(TCHAR));
		if (command == NULL) {
			printf("Error allocating memory");
			free(command_chr);
			return -1;
		}
		mbstowcs(command, command_chr, len_tot);
	}
	else
	{	
		command = command_chr;
	}
	retVal = CreateProcessSimple(command, &procinfo);


	if (retVal == 0)
	{
		printf("Process Creation Failed!\n");
		if (flag_size_tchar_2)
		{
			free(command);
		}
			free(command_chr);
		return -1;
	}


	waitcode = WaitForSingleObject(
		procinfo.hProcess,
		TIMEOUT_IN_MILLISECONDS); /* Waiting 5 secs for the process to end */

	printf("WaitForSingleObject output: ");
	switch (waitcode)
	{
	case WAIT_TIMEOUT:
		printf("WAIT_TIMEOUT\n"); break;
	case WAIT_OBJECT_0:
		printf("WAIT_OBJECT_0\n"); break;
	default:
		printf("0x%x\n", waitcode);
	}

	if (waitcode == WAIT_TIMEOUT) /* Process is still alive */
	{
		printf("Process was not terminated before timeout!\n"
			"Terminating brutally!\n");
		TerminateProcess(
			procinfo.hProcess,
			BRUTAL_TERMINATION_CODE); /* Terminating process with an exit code of 55h */
		Sleep(10); /* Waiting a few milliseconds for the process to terminate,
					note the above command may also fail, so another WaitForSingleObject is required.
					We skip this for brevity */
	}

	GetExitCodeProcess(procinfo.hProcess, &exitcode);

	printf("The exit code for the process is 0x%x\n", exitcode);


	CloseHandle(procinfo.hProcess); /* Closing the handle to the process */
	CloseHandle(procinfo.hThread); /* Closing the handle to the main thread of the process */
	if (flag_size_tchar_2) {
		free(command);
	}
	free(command_chr);
	return (int)exitcode;
}



BOOL CreateProcessSimple(LPTSTR CommandLine, PROCESS_INFORMATION* ProcessInfoPtr)
{
	STARTUPINFO	startinfo = { sizeof(STARTUPINFO), NULL, 0 }; /* <ISP> here we */
															  /* initialize a "Neutral" STARTUPINFO variable. Supplying this to */
															  /* CreateProcess() means we have no special interest in this parameter. */
															  /* This is equivalent to what we are doing by supplying NULL to most other */
															  /* parameters of CreateProcess(). */

	return CreateProcess(
		NULL, /*  No module name (use command line). */
		CommandLine,			/*  Command line. */
		NULL,					/*  Process handle not inheritable. */
		NULL,					/*  Thread handle not inheritable. */
		FALSE,					/*  Set handle inheritance to FALSE. */
		NORMAL_PRIORITY_CLASS,	/*  creation/priority flags. */
		NULL,					/*  Use parent's environment block. */
		NULL,					/*  Use parent's starting directory. */
		&startinfo,				/*  Pointer to STARTUPINFO structure. */
		ProcessInfoPtr			/*  Pointer to PROCESS_INFORMATION structure. */
	);
}







