
#ifndef  __task_H__
#define __task_H__


#define _CRT_SECURE_NO_WARNINGS 1

#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include "main.h"
#include "HardCodedData.h"
#include "windows.h"



typedef struct forest {
	FILE* input;
	FILE* output;
	int demention;
	int roots;
	int num_burning_tree;
	char *forest;
	char *forest_gen;
}Forest ;

// enum////
enum os_error { No_Error,Error_letter, Error_demention };



////function decleration/////

/// burning_trees
/// function that opens the son process and count the number of burning trees
/// <param name= struct forest *env
/// returns number of burning trees
int burning_trees(struct forest* env);


/// CreateProcessSimple
/// create the handle to son process
/// PARAM:
/// LPTSTR "CommandLine"
/// PROCESS_INFORMATION* "ProcessInfoPtr"
/// return the handle of son process

BOOL CreateProcessSimple(LPTSTR CommandLine, PROCESS_INFORMATION* ProcessInfoPtr);

/// CreateProcessSimpleMain
/// execute the son process and waits until finish
/// param:
/// char *"p_str"
/// return the value from the son process
int CreateProcessSimpleMain(char* p_str);

/// init_data
/// initialize the fater process data
/// param:
/// struct forest* env
/// int argc
/// char** argv
void init_data(struct forest* env, int argc, char** argv);

/// file2data
/// convert the .txt data to struct forest fields
///param:
/// forest
/// return the struct of forest
char *file2data(struct forest *env);

/// gen_fire
/// check the next generation of FIRE and put in next gen  
/// struct forest* env
/// int place
void gen_fire(struct forest* env, int place);

/// gen_ground
/// check the next generation of GROUND and put in next gen
/// struct forest* env
/// int place
void gen_ground(struct forest* env, int place);

/// gen_tree
/// check the next generation of TREE and put it in next gen
/// struct forest* env
/// int place
void gen_tree(struct forest* env, int place);

/// gen
/// calls the appropriate gen function
//struct forest* env
/// int place
void gen(struct forest* env, int place);

/// gen_forest
/// buildes the acctual next generation forest as a string 
/// calls curning trees function 
/// plot the final output file at argv[2] 
/// struct forest *env
void gen_forest(struct forest *env);

/// freeall
/// frees all allocated memory
/// struct env
void freeall(struct forest *env);

#endif // __task_H__