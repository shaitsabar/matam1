
/// Count_Burning_Tree
/// 
/// counts burning trees in forest
/// params:
/// string, int.
/// returs the number of burning trees in forest
int Count_Burning_Tree(char str[], int size);

//variable declaration
#define BURNING_TREE 'F'
