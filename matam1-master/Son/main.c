#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include "task.h"
#include "main.h"


int main(int argc, char *argv[])
{
	int size = strlen(argv[1]);
	int count = 0;
	char *str =(char*) malloc(sizeof(char) * size);
	if (str == NULL)
	{
		printf("Error memory allocation");
		return -1;
		
	}
	strcpy_s(str, (size_t)(size)+1, argv[1]);
	printf(str);

	count = Count_Burning_Tree(str,size);
	printf("%d", count);
	return count; 
}
